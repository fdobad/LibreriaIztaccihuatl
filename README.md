Ejecuta el informe on line en binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://hub.gke2.mybinder.org/user/fdobad-libreriaiztaccihuatl-ab8a1ek1/doc/tree/informe.ipynb)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fdobad%2FLibreriaIztaccihuatl/9a047ba469596d7f9f69b348bd69fd823288a158?urlpath=lab%2Ftree%2Finforme.ipynb)

# Portada: Utilizando la Ciencia de Datos en una Librería Iztaccihuatl

<!-- #region jp-MarkdownHeadingCollapsed=true tags=[] -->
# Indice
1. Portada
1. Indice
1. Introduccion
1. Comprension del negocio
1. Adquisicion de datos
1. Preparacion y exploración de datos
1. Modelacion e identificacion de indicadores
1. Justificación, evaluación e interpretación
1. Propuesta
1. Despliegue y Operaciones
1. Conclusiones
1. Anexos
<!-- #endregion -->

# Introduccion
Se presenta un estudio de un caso aplicado a la Librería Iztaccihuatl ubicada en la ciudad de Monterrey, Nuevo León, México, cuyo objetivo es generar un modelo basado en Ciencia de Datos como apoyo a la toma de decisiones. Se analizan las dos fuentes de información entregadas: la introducción y los 4 archivos de datos de esta organización.

Se aplica la metodología standard de proyecto de datos CRISP-DM para dividir el proyecto en etapas; con el objetivo general de comprender su negocio, encontrar indicadores, aplicar un modelo y crear una estratégia con recomendaciónes para generar valor a esta libreria.

Este estudio de caso pretende transversalmente responder 3 preguntas:

    ¿Qué indicadores serían los más importantes a determinar de acuerdo a la información presentada?
    ¿Qué tipo de análisis sería el más adecuado y por qué?
    ¿Qué decisiones se podrían tomar basadas en los descubrimientos o inferencias de la información analizada?


<!-- #region jp-MarkdownHeadingCollapsed=true tags=[] -->
# Comprension del negocio
De la información entregada se entiende que la libreria es de tamaño mediano (5) tipo hibrida en el sentido que vende libros en papel y no virtuales (1), pero tiene un contacto muy estrecho con el cliente para mantener listados de lo que los clientes han leido (con su rating asociado, (2)) y quieren leer (3). También la contabilidad entregada es limitada a los libros más vendidos (4).  

(1) : ver tabla top_books columna Bindings de valores 'paperback' o 'hardback'  
(2) : ver tabla ratings  
(3) : ver tabla to_read  
(4) : ver tabla top_books  
(5) : ver tabla books con 10 mil libros distintos  
<!-- #endregion -->

<!-- #region tags=[] -->
# Adquisicion de datos
Basicamente existe una sola fuente de datos: la web de la entrega del caso; No es necesario hacer entrevistas a diversas personas de la organización o documentar accesos a diferentes sistemas o bases de datos, como ocurriría en la realidad.
- Dado esto se __clasifican__ dos fuentes: la descripción (el texto) y la información (los cuatro archivos descargables)
- Previa a su ingesta digital se escanean los links por si tienen algun enlace __malicioso o virus__, utilizando una herramienta online como [virustotal](https://www.virustotal.com/gui/home/url). Los archivos son seguros.
- El archivo ratings, no tiene __formato__ de texto plano como los demás, luego es necesario descomprimirlo antes de usarlo
```bash
$ unzip ratings.zip
```
- Una medición de __calidad de los datos__ es la siguiente: Se comprobó que las cabeceras de los datos descritos en el texto, efectivamente correspondían a las de los archivos.
<!-- #endregion -->

<!-- #region tags=[] -->
# Preparacion y exploración de datos
Se cargan los datos digitalmente y se observa __limpieza, faltantes o inconsistencias__ en una primera exploración automática
<!-- #endregion -->

```python tags=[]
# cargar cada tabla como un DataFrame
import lux
import pandas as pd
import numpy as np
nombres = ['books','ratings','top_books','to_read']                                                                                                                                                                                                            
df = dict(zip(nombres,map(lambda x: pd.read_csv(x+'.csv'),nombres)))   
```

### total de datos

```python
espacio=[]
missing=[]
print('tabla\tespacio\tfaltantes\t%faltantes')
for t in df:
    espacio += [ len(df[t])*len(df[t].columns) ]
    missing += [ np.sum( np.sum( df[t].isna() )) ]
    print(t, espacio[-1], missing[-1], "{:.2f}".format(100*missing[-1]/espacio[-1]), sep='\t')
print('total:', sum(espacio), sum(missing), "{:.2f}".format(100*sum(missing)/sum(espacio)), sep='\t')
```

Se observa que de los 4.9 millones de registros los faltantes son menos del 0.06%


## books

```python
# ver informacion resumen: nombre de columna, faltantes y formato
df['books'].info()
```

<!-- #region tags=[] -->
De esta tabla se observa que para 5 de las 12 columnas __falta__ menos del 10% de la información.  
En total falta el 2.29% como se muestra en la tabla anterior, lo que no es tan problemático.  
Específicamente los códigos __ISBN, ISBN13, año de publicacion y el titulo original__, se aprecia que es menos de 10%, lo que no debería mayores problemas.  
Salvo para __language_code__ que al notar que la mayoría de ellos son en inglés _se debería asumir que los faltantes son del mismo idioma ingles_.
<!-- #endregion -->

```python tags=[]
# ver estadisticos descriptivos de las columnas numericas
df['books'].describe()
```

<!-- #region tags=[] -->
Aca solo el __año de publicacion y rating__ son datos sus estadisticos clasicos aportan informacion:  
    - el año de publicacion indica que la mayoria (3/4) de los libros son actuales (2004 en adelante), y el minimo de 1750 antes de cristo sorprende pero al buscarlo en internet efectivamente se estima es de esa epoca.  
    - El rating es un numero entre 2.47 y 4.82, con promedio 4. Luego se infiere una comportamiento de calificación de los usuarios.
<!-- #endregion -->

```python tags=[]
# ver estadisticos exploratorios de datos automaticamente sugeridos por lux-api
# presione el boton toggle para cambiar entre los datos y los gráficos
df['books']
```

## top books

```python tags=[]
# ver informacion resumen: nombre de columna, faltantes y formato
df['top_books'].info()
```

Se aprecia que sólo __faltan 4 autores__

```python
# corrigiendo un warning de la fecha
df['top_books']['Publ Date'] = pd.to_datetime(df['top_books']['Publ Date'], format='%d %b %Y')
```

```python tags=[]
df['top_books'][ df['top_books'].Author.isnull() ]
```

<!-- #region tags=[] -->
Situacion facilmente corregible buscando en la web, e insertando en la base
<!-- #endregion -->

```python tags=[]
df['top_books'].Author.iloc[74] = 'Sara Lewis' # https://www.amazon.com/Slow-Cooker-Recipes-Hamlyn-Color/dp/0600633632
df['top_books'].Author.iloc[81] = 'Guinness World Records' # https://www.amazon.com/Guinness-World-Records-2011/dp/190499458X
df['top_books'].Author.iloc[115] = 'Based on Rev Awdry character' # https://www.amazon.com/Thomas-Rescue-Friends-Based-character/dp/1405251115
df['top_books'].Author.iloc[118] = 'D C Thomson & Co' # https://www.amazon.co.uk/Beano-Annual-2011-Thomson-Co/dp/1845354141 
```

<!-- #region tags=[] -->
Explorando más a fondo si existe una clasificación que se venda más
<!-- #endregion -->

```python
df['top_books']
```

```python tags=[]
df['top_books'].intent = ['Value','Classification']
```

```python tags=[]
df['top_books']
```

Se aprecia que los libros bajo la clasificación de 'HB Non Fiction' y 'PB Fiction' son los con mayor valor promedio por lo que deberían ser especialmente considerados.
Calculando su valor acumulado:

```python
df['top_class'] = df['top_books'].groupby('Classification').agg({'Value':'sum'})
df['top_class'].sort_values('Value', ascending=False, inplace=True)
df['top_class']
```

Se llega a la misma conclusión, esta librería tiene dos clasificaciones más rentables: 'HB Non Fiction' y 'PB Fiction'


## ratings

```python tags=[]
df['ratings'].info()
```

```python tags=[]
df['ratings']
```

## to read

```python
df['to_read'].info()
```

```python tags=[]
df['to_read']
```

<!-- #region tags=[] -->
### Consistencia entre tablas
Se chequea si el modelo de datos liga las tablas correctamente y si existen llaves faltantes

- La tabla central del modelo de datos, es `books`, liga a:

  - `top_books`, por la llave `isbn`
  - `ratings`, por la llave `book_id`
  - `to_read`, por la llave `book_id`  
<!-- #endregion -->

<!-- #region tags=[] -->
- Se verifica que todos los registros de `ratings` y `to_read` tengan un `book_id` en `books`
<!-- #endregion -->

```python tags=[]
def missingList(li, se):
    notIn=[]
    for l in li:
        if l not in se:
            notIn+=[l]
    return notIn

print(missingList( df['ratings'].book_id.unique(), df['books'].book_id ))
print(missingList( df['to_read'].book_id.unique(), df['books'].book_id ))
```
<!-- #region tags=[] -->
Se aprecia que el `book_id == 10000` no existe en `books`, pero si en `ratings` y `to_read`
<!-- #endregion -->

```python tags=[]
print(missingList( df['top_books'].ISBN.unique(), df['books'].isbn))
print(missingList( df['top_books'].ISBN.unique(), df['books'].isbn13))
```

<!-- #region tags=[] -->
Se aprecia que el libro de `isbn == 9780000000000` no existe en `books`, pero si en `top_books`
<!-- #endregion -->

```python tags=[]
len(df['top_books'][ df['top_books'].ISBN == 9780000000000 ])
```

Se aprecia que hay 120 libros marcados con el mismo ISBN lo que claramente es un error debido a que este dato debe ser único.  
- Cuantificando mejor esta diferencia, se compara si hay misma cantidad de registros que de llaves (registros únicos)

```python
len(df['books'].book_id.unique()) - len(df['books'])
```

```python
len(df['books'].isbn.unique()) - len(df['books'])
```

```python
len(df['books'].isbn13.unique()) - len(df['books'])
```

Se aprecia que `books_id` es consistente, pero no así `isbn` y `isbn13` que tienen 699 y 9986 valores por corregir.

Estas inconsistencias seran ignoradas en lo posible: 
- no relacionando a traves de `isbn` (entre `books` y `top_books`)
- dejando el libro de `bookid == 10000` fuera del analisis

Pero la estrategia futura sera dejar pendiente la consultada al cliente para que resuelva este problema encontrando los datos faltantes

```python
# eliminar el registro
df['ratings'] = df['ratings'][ df['ratings'].book_id!=10000 ]
df['to_read'] = df['to_read'][ df['to_read'].book_id!=10000 ]
```

<!-- #region tags=[] -->
# Modelacion e identificacion de indicadores

De acuerdo a la información presentada que ha sido explorada en la sección anterior, podemos plantear varios de los indicadores más relevantes para mejorar la toma de decisiones del negocio.

## Listado de indicadores

Relacionado a los lectores:  

    1. Indicador de intensidad de lectura: 
        1.a. Cuáles usuarios leen más
        1.b. Rankean más lo que leen
        1.c. Ambas acciones a la vez.  

Relacionado a los libros:  

    2. El indicador de cuáles son los libros más:
        2.a. Deseados por leer  
        2.b. Deseados por rankeados y por releer
    3. Cuáles y qué tipo son los que generan más valor en su venta 
        3.a. Cuáles libros generan más valor o margen
        3.b. Qué tipo son los que generan más valor en su venta  
<!-- #endregion -->

<!-- #region tags=[] -->
### 1.a Grupo de __super lectores__
Para encontrarlos se calcula la frecuencia -de los usuarios- y se ordena la tabla de los ratings.  
Y se toma solo el grupo de 2 desviaciones std más rankeador (o el percentil 2.5% superior)
<!-- #endregion -->

```python
df['super_rater_users'] = df['ratings'].groupby('user_id').agg({'user_id':'count'})
df['super_rater_users'].rename(columns={'user_id':'count'}, inplace=True)
df['super_rater_users'].sort_values('count', inplace=True, ascending=False)
df['super_rater_users'].describe()
```

```python
cutoff = df['super_rater_users']['count'].mean() + 2 * df['super_rater_users']['count'].std()
df['super_rater_users'] = df['super_rater_users'][ df['super_rater_users']['count'] > cutoff ]
df['super_rater_users']
```

Se ha encontrado un listado de los 2675 lectores que más han rankeado libros -al menos 71 ratings: __súper lectores__


### 1.b Grupo de __super deseadores__
Para encontrarlos se calcula la frecuencia -de los usuarios- y se ordena la tabla de los libros por leer.  
Y se toma solo el grupo de 2 desviaciones std más rankeador (o el percentil 2.5% superior)  

```python tags=[]
df['super_to_read_users'] = df['to_read'].groupby('user_id').agg({'user_id':'count'})
df['super_to_read_users'].rename(columns={'user_id':'count'}, inplace=True)
df['super_to_read_users'].sort_values('count', inplace=True, ascending=False)
df['super_to_read_users'].describe()
```

mostrando solo el grupo de 2 desviaciones std mas rankeador (o de percentil 2.5%)

```python
cutoff = df['super_to_read_users']['count'].mean() + 2 * df['super_to_read_users']['count'].std()
df['super_to_read_users'] = df['super_to_read_users'][ df['super_to_read_users']['count'] > cutoff ]
df['super_to_read_users']
```

Se ha encontrado un listado de los 2555 lectores que más han marcado libros por leer -al menos 51: __súper deseadores__


### 1.c. Ambas acciones a la vez
Se intersectan ambos listados para conocer si hay usuarios que a la vez son los que más deasean y leen

```python tags=[]
df['super_users'] = df['super_rater_users'].merge( df['super_to_read_users'], on=['user_id'], how='inner')
df['super_users']
```

Se aprecia que no existe intersección entre ambos súper usuarios, en este momento.

```python
# no le creo
notFound=0
for ui in df['super_rater_users'].index:
    if not ui in df['super_to_read_users'].index:
        notFound+=1
print(notFound,notFound == len(df['super_rater_users'].index))
```

```python
# no le creo
notFound=0
for ui in df['super_to_read_users'].index:
    if not ui in df['super_rater_users'].index:
        notFound+=1
print(notFound,notFound == len(df['super_to_read_users'].index))
```

```python
# ahora si le creo
```

### 2.a. Grupo de libros más deseados por leer  
Para encontrarlos se calcula la frecuencia -de los libros- y se ordena la tabla de los libros por leer.  
Y se toma solo el grupo de 2 desviaciones std más rankeador (o el percentil 2.5% superior)  

```python
df['most_to_read'] = df['to_read'].value_counts('book_id')
df['most_to_read'].describe()
```

```python
cutoff = df['most_to_read'].mean() + 2 * df['most_to_read'].std()
df['most_to_read'][ df['most_to_read'] >= cutoff ]
```

Tomando el percentil 2.5% se encuentra un listado de 385 libros que son los más deseados por leer

<!-- #region tags=[] -->
### 2.b. Grupo de libros rankeados y por leer a la vez
<!-- #endregion -->

<!-- #region jp-MarkdownHeadingCollapsed=true tags=[] -->
Existen 2 posibilidades:
- Que lo leyó, rankeó y quiere repetir su lectura. El ranking compara libros leídos.
- Que no lo leyó y rankeó para comparar cuales libros quisiera leer más que otros.  
<!-- #endregion -->

```python tags=[]
# equivalente a inner join
df['ranked_to_read'] = df['to_read'].merge( df['ratings'], on=['user_id','book_id'], how='inner')
df['ranked_to_read']
```

```python
# repitieron ranking y por leer?
if len( df['ranked_to_read'].user_id.unique() ) == len(df['ranked_to_read']):
    print('no hay usuarios con mas de un registro rankeado y por leer')
else:
    print('Existen usuarios que rankearon mas de un libro por leer\nSe priorizara tomando unicamente el de mayor ranking')
    df['ranked_to_read'].sort_values(ascending=False,by=['user_id','rating'],inplace=True)
    df['best_ranked_to_read'] = df['ranked_to_read'].groupby('user_id').first().reset_index() 
```

```python
df['best_ranked_to_read']
```

Se encontró listado de 234 libros asociados a un usuario particular


Nota: Si solo se busca el listado de libros de mayor frecuencia de esta sub-lista, los resultados son demasiado pocos para ser utiles

```python
df['best_ranked_to_read'].value_counts('book_id')[ df['best_ranked_to_read'].value_counts('book_id') > 1 ]
```

### 3.a. Cuáles libros generan más valor o margen


Si:

    Volume - Volumen de ventas hasta el 2010
    Value - Ventas determinadas por el volumen
    RRP - Precio recomendado para minoristas
    ASP - Precio promedio para venta
    
Entonces se puede derivar
    
    Venta promedio por unidad = Value / Volume
    
Que al calcular, es equivalente a `ASP`, salvo para 5 libros mostrados a continuacion.
Asi para estos, se puede

```python tags=[]
df['top_books']['ValUnit'] = df['top_books'].Value / df['top_books'].Volume
df['top_books'][ abs(df['top_books'].ValUnit - df['top_books'].ASP) > 0.1] 
```

Asi mismo, mientras mayor es el retorno mas rentable es de vender

```python tags=[]
df['top_books']['profit'] = df['top_books'].RRP - df['top_books'].ASP
df['top_books'].sort_values('profit',ascending=False)
```

Así se encuentran los listados de prioridades para libros que generarán más valor o margen

<!-- #region tags=[] -->
### 3.b. Qué tipo son los que generan más valor en su venta  
Estos ya se encontraron en la sección de exploración de datos, al ordenar por frecuencia de clasificación:
<!-- #endregion -->

```python
df['top_class']
```

De donde se aprecia que cada una de las primeras dos categorías generan casi el doble que la categoría 3era del listado: 'HB Non Fiction' y 'PB Fiction'

<!-- #region jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] -->
# Justificación, evaluación e interpretación

## Justificación

De acuerdo a los datos explorados, la ciencia de datos con sus análisis descriptivos es la metodología que más se ajusta a la situación del caso entregado. Son demasiados datos para ser análizados sin métodos estadísticos; y a su vez la estructura de los datos al no poseer fechas de las actividades, tampoco se presta para análisis predictivo. Finalmente, aunque de manera rudimentaria, al encontrar un listado de lectores o libros importantes, estos son un insumo o lista de prioridades prescriptiva para acciones futuras por parte del negocio como una campaña de marketing o reposición de libros para su venta.

## Evaluación
Se lograron encontrar los indicadores planteados, alrededor de los usuarios y -debido a datos faltantes- con un poco menos de precisión para los libros.

## Interpretación

Al haber encontrado los indicadores planteados, se puede plantear un resumen de ellos para interpretarlos:

| Indice | Indicador | principal | miembros o largo |
| -- | -- | -- | -- |
| 1.a. | súper lectores | lector | 2675 |
| 1.b. | súper deseadores | lector | 2555 |
| 1.c. | Ambas | lector | 0 |
| 2.a | Más desados por leer | libro | 385 |
| 2.b. | Deseados por rankeados y por releer | libro | 234 |
| 3.a. | Generan más valor o margen | libro | 10000 |
| 3.b. | Clasificación que generan más valor | libro | 2 |

Se interpreta claramente que los indicadores 1, están centrados en el usuario. Los 2 mezclan información del usuario para estar centrados en el libro, y los tipo 3 están totalmente centrados en los libros.

También debido a la cantidad de miembros en cada tabla del indicador encontrado, cabe de cajón la estrategia:
- Si son muy pocos miembros la estrategia debe ser directa y aplicada: Por ejemplo la 3, debe ser acatada directamente
- Si son cientos de libros, la estrategia puede ser más relajada: Por ejemplo planificar dividir en 12 y tener cada mes del año un grupo de libros en descuento
- Si son miles, entonces la estrategia debe ser un marketing digital y directa: Contactar a cada usuario via mail o redes sociales para ofrecerle el libro que eligió.

# Propuesta

La propuesta para la librería es una de marketing y reposición.

Con respecto al marketing, la librería debe hacer dos acciones:
- Marketing de promociones o descuentos ṕara los miembros de los indicadores tipo 2. Es decir, para los libros más deseados por leer y para los de las categorías que más valor agregan. Realizando por ejemplo un banner en la landing page del e-commerce o un cartel físico en tienda, que sea reemplazado mes a mes por una proporción de estos, para mantener la novedad de la promoción
- Marketing directo para los grupos de súper lectores, súper deseadores, su conjunción. Para los indicadores tipo 1, lo mejor es contactarlos via redes sociales.

Con respecto a la reposición de libros:
- Priorización a los grupos editoriales y temáticas de libros que más valor agregan, usando el indicador 3.b

# Despliegue y Operaciones

Para llevar a cabo esta propuesta, es necesario llevar varias acciones: 
- Publicidad física en tienda, 
- Publicidad virtual en el e-commerce 
- Marketing directo en redes sociales.
- Prorización de libros al planificar reposición de inventarios.

Así se estima que deben estar las siguientes capacidades en la empresa:
- poder externalizar el diseño e impresión del material físico para la publicidad.  
- poder modificar la web del e-commerce
- poder realizar o externalizar una campaña de marketing directo
- poder regenerar este documento de jupyter notebook para actualizar los indicadores (copiar los archivos csv y ejecutar este documento)
- poder priorizar un listado de compras con el indicador 3.a.

# Conclusiones

Se entrega una estrategia basada en Ciencia de Datos para la librería a fin de mejorar sus funciones de publicidad, marketing y reposición. Se realiza la recomendación llevando adelante una metodología CRISP-DM, la cual sirvió para separar la investigación en etapas y estandarizar la búsqueda para su fácil justificación y sobre todo, para poder conformar una estrategia repetible.
<!-- #endregion -->

<!-- #region jp-MarkdownHeadingCollapsed=true tags=[] -->
# Anexos
<!-- #endregion -->

## Modelo de datos
El archivo “books” contiene los datos generales de cada libro existente en la librería y además menciona el promedio de clasificación de cada libro de acuerdo a las votaciones y compras del cliente.  
El archivo “top_books” contiene el top 20 de los libros más vendidos de acuerdo a una clasificación general.  
El archivo “ratings” contiene los datos de los libros más votados por los clientes dentro del sitio web de la librería.  
El archivo “to_read” contiene las recomendaciones que cada cliente o usuario realiza en el sitio web sobre libros para leer.  

El archivo “books” contiene los siguientes datos:

    Id - Identificador del registro
    Book Id - Identificador del libro
    Number Editions - Número de ediciones
    ISBN - Clave estándar internacional del libro
    ISBN13 - Clave estándar extendida internacional del libro
    Authors - Autor del libro
    Original Publication - Fecha de publicación
    Original Title - Título original del libro
    Title - Título del libro
    Language Code - Clave de idioma del libro
    Average Rating - Promedio de la clasificación del libro
    Image - Enlace a la imagen de la portada del libro
    Small Image - Enlace a la imagen en versión optimizada de la portada del libro.

El archivo “top_books” contiene los siguientes datos:

    Position - Posición del libro en la clasificación del libro
    ISBN - Clave estándar extendida internacional del libro
    Title - Título del libro
    Author - Autor del libro
    Imprint - Editorial
    Publisher Group - Grupo Editorial
    Volume - Volumen de ventas hasta el 2010
    Value - Ventas determinadas por el volumen
    RRP - Precio recomendado para minoristas
    ASP - Precio promedio para venta
    Binding - Tipo de encuadernación
    Publ Date - Fecha de publicación
    Product Class - Clasificación del libro
    Classification - Clasificación General del libro

El archivo “ratings” contiene los siguientes datos:

    Book Id - Identificador del libro
    User Id - Identificador del cliente/usuario que clasifico un libro
    Rating - Nivel de clasificación del libro.

El archivo “to_read” contiene los siguientes datos:

    User Id - Identificador del cliente/usuario que clasifico un libro
    Book Id - Identificador del libro



## Instrucciones del Caso
Requerimientos
1. ¿Qué indicadores serían los más importantes a determinar de acuerdo a la información presentada? al menos 3 indicadores de desempeño (kpi's)
2. ¿Qué tipo de análisis sería el más adecuado y por qué? Justificar el tipo de análisis 
3. ¿Qué decisiones se podrían tomar basadas en los descubrimientos o inferencias de la información analizada? Para cada indicador de desempeño determinar al menos una decisión que debería tomar la librería. 

Partes del informe
- portada; 
- un índice 
- Introducción, en donde presentes las ideas que se revisarán en el proyecto.
- Identificar los indicadores.
- Seleccionar el tipo de analizar y justificar su uso.
- Elaborar una propuesta de decisiones que la empresa podrá tomar en base a la información analizada.
- Conclusiones, con un resumen de las propuestas que presentaste a la empresa.

Cumplir con los requisitos establecidos en la Lista de cotejo señalada en la descripción de la actividad.
1. Contiene los datos de identificación de la empresa.		
2. Se incluye la descripción detallada de la estrategia de implementación.		
3. La estrategia de implementación contempla un proceso de evaluación. 		
4. Se mencionan las buenas prácticas que se deben utilizar para lograr la estrategia descrita. 		
5. Se justifica la estrategia de implementación. 

Evaluar la práctica de 5 compañeros, los cuales te serán asignados automáticamente por la plataforma. Si no cumples este requisito no se liberará la evaluación de tu práctica individual.

```python

```
