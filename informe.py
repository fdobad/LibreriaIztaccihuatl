# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Portada: Utilizando la Ciencia de Datos en una Librería Iztaccihuatl

# + [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# # Indice
# 1. Introduccion
# 1. Comprension del negocio
# 1. Adquisicion de datos
# 1. Preparacion de datos
# 1. Modelacion a identificacion de indicadores
# 1. Evaluacion, interpretacion y justificacion
# 1. Propuesta
# 1. Despliegue y Operaciones
# 1. Conclusiones
# 1. Anexos
# -

# # Introduccion
# Se presenta un estudio de un caso aplicado a la Librería Iztaccihuatl ubicada en la ciudad de Monterrey, Nuevo León, México, cuyo objetivo es generar un modelo basado en Ciencia de Datos como apoyo a la toma de decisiones. Se analizan las dos fuentes de información entregadas: la introducción y los 4 archivos de datos de esta organización.
#
# Se aplica la metodología standard de proyecto de datos CRISP-DM para dividir el proyecto en etapas; con el objetivo general de comprender su negocio, encontrar indicadores, aplicar un modelo y crear una estratégia con recomendaciónes para generar valor a esta libreria.

# + [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# # Comprension del negocio
# De la información entregada se entiende que la libreria es de tamaño mediano (5) tipo hibrida en el sentido que vende libros en papel y no virtuales (1), pero tiene un contacto muy estrecho con el cliente para mantener listados de lo que los clientes han leido (con su rating asociado, (2)) y quieren leer (3). También la contabilidad entregada es limitada a los libros más vendidos (4).  
#
# (1) : ver tabla top_books columna Bindings de valores 'paperback' o 'hardback'  
# (2) : ver tabla ratings  
# (3) : ver tabla to_read  
# (4) : ver tabla top_books  
# (5) : ver tabla books con 10 mil libros distintos  

# + [markdown] tags=[]
# # Adquisicion de datos
# Basicamente existe una sola fuente de datos: la web de la entrega del caso; No es necesario hacer entrevistas a diversas personas de la organización o documentar accesos a diferentes sistemas o bases de datos, como ocurriría en la realidad.
# - Dado esto se __clasifican__ dos fuentes: la descripción (el texto) y la información (los cuatro archivos descargables)
# - Previa a su ingesta digital se escanean los links por si tienen algun enlace __malicioso o virus__, utilizando una herramienta online como [virustotal](https://www.virustotal.com/gui/home/url). Los archivos son seguros.
# - El archivo ratings, no tiene __formato__ de texto plano como los demás, luego es necesario descomprimirlo antes de usarlo
# ```bash
# $ unzip ratings.zip
# ```
# - Una medición de __calidad de los datos__ es la siguiente: Se comprobó que las cabeceras de los datos descritos en el texto, efectivamente correspondían a las de los archivos.

# + [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# # Preparacion de datos
# Se cargan los datos digitalmente y se observa __limpieza, faltantes o inconsistencias__ en una primera exploración automática

# + tags=[]
# cargar cada tabla como un DataFrame
#import lux
import pandas as pd                                                                                                                               
nombres = ['books','ratings','top_books','to_read']                                                                                                                                                                                                            
df = dict(zip(nombres,map(lambda x: pd.read_csv(x+'.csv'),nombres)))   
# -

# ## books

# ver informacion resumen: nombre de columna, faltantes y formato
df['books'].info()

# + [markdown] tags=[]
# De esta tabla se observa que para 5 de las 12 columnas __falta__ menos del 10% de la información.  
# Específicamente los códigos __ISBN, ISBN13, año de publicacion y el titulo original__, se aprecia que es menos de 10%, lo que no debería mayores problemas.  
# Salvo para __language_code__ que al notar que la mayoría de ellos son en inglés _se debería asumir que los faltantes son del mismo idioma ingles_.

# + jupyter={"source_hidden": true} tags=[]
# ver estadisticos descriptivos de las columnas numericas
df['books'].describe()

# + [markdown] tags=[]
# Aca solo el __año de publicacion y rating__ son datos sus estadisticos clasicos aportan informacion:  
#     - el año de publicacion indica que la mayoria (3/4) de los libros son actuales (2004 en adelante), y el minimo de 1750 antes de cristo sorprende pero al buscarlo en internet efectivamente se estima es de esa epoca.  
#     - El rating es un numero entre 2.47 y 4.82, con promedio 4. Luego se infiere que siempre son bien calificados

# + jupyter={"source_hidden": true} tags=[]
# ver estadisticos exploratorios de datos automaticamente sugeridos por lux-api
# presione el boton toggle para cambiar entre los datos y la sugerencia
df['books']
# -

# ## top books

# + tags=[]
# ver informacion resumen: nombre de columna, faltantes y formato
df['top_books'].info()
# -

# Se aprecia que sólo __faltan 4 autores__

# + jupyter={"source_hidden": true} tags=[]
df['top_books'][ df['top_books'].Author.isnull() ]

# + [markdown] tags=[]
# Situacion facilmente corregible buscando en la web, e insertando en la base

# + jupyter={"source_hidden": true} tags=[]
df['top_books'].Author.iloc[74] = 'Sara Lewis' # https://www.amazon.com/Slow-Cooker-Recipes-Hamlyn-Color/dp/0600633632
df['top_books'].Author.iloc[81] = 'Guinness World Records' # https://www.amazon.com/Guinness-World-Records-2011/dp/190499458X
df['top_books'].Author.iloc[115] = 'Based on Rev Awdry character' # https://www.amazon.com/Thomas-Rescue-Friends-Based-character/dp/1405251115
df['top_books'].Author.iloc[118] = 'D C Thomson & Co' # https://www.amazon.co.uk/Beano-Annual-2011-Thomson-Co/dp/1845354141 

# + tags=[]
df['top_books']
# -

# ## ratings

# + tags=[]
df['ratings'].info()

# + jupyter={"source_hidden": true} tags=[]
df['ratings']
# -

# ## to read

df['to_read'].info()

# + tags=[]
df['to_read']

# + [markdown] tags=[]
# ### Consistencia entre tablas
# Se chequea si el modelo de datos liga las tablas correctamente y si existen llaves faltantes
#
# - La tabla central del modelo de datos, es `books`, liga a:
#
#   - `top_books`, por la llave `isbn`
#   - `ratings`, por la llave `book_id`
#   - `to_read`, por la llave `book_id`  

# + [markdown] tags=[]
# - Se verifica que todos los registros de `ratings` y `to_read` tengan un `book_id` en `books`

# + tags=[]
def missingList(li, se):
    notIn=[]
    for l in li:
        if l not in se:
            notIn+=[l]
    return notIn

print(missingList( df['ratings'].book_id.unique(), df['books'].book_id ))
print(missingList( df['to_read'].book_id.unique(), df['books'].book_id ))
# + [markdown] tags=[]
# Se aprecia que el `book_id == 10000` no existe en `books`, pero si en `ratings` y `to_read`

# + tags=[]
print(missingList( df['top_books'].ISBN.unique(), df['books'].isbn))
print(missingList( df['top_books'].ISBN.unique(), df['books'].isbn13))

# + [markdown] tags=[]
# Se aprecia que el libro de `isbn == 9780000000000` no existe en `books`, pero si en `top_books`

# + tags=[]
len(df['top_books'][ df['top_books'].ISBN == 9780000000000 ])
# -

# Se aprecia que hay 120 libros marcados con el mismo ISBN lo que claramente es un error debido a que este dato debe ser único.  
# - Cuantificando mejor esta diferencia, se compara si hay misma cantidad de registros que de llaves (registros únicos)

len(df['books'].book_id.unique()) - len(df['books'])

len(df['books'].isbn.unique()) - len(df['books'])

len(df['books'].isbn13.unique()) - len(df['books'])

# Se aprecia que `books_id` es consistente, pero no así `isbn` y `isbn13` que tienen 699 y 9986 valores por corregir.
#
# Estas inconsistencias seran ignoradas en lo posible: 
# - no relacionando a traves de `isbn` (entre `books` y `top_books`)
# - dejando el libro de `bookid == 10000` fuera del analisis
#
# Pero la estrategia futura sera dejar pendiente la consultada al cliente para que resuelva este problema

# eliminar el registro
df['ratings'] = df['ratings'][ df['ratings'].book_id!=10000 ]
df['to_read'] = df['to_read'][ df['to_read'].book_id!=10000 ]

# + [markdown] tags=[]
# # Modelacion a identificacion de indicadores
# ## Hipótesis
# 1. ratings, to read JOINS to top_books -> asociar genero o Classification, o autor de leidos por usuario -> tomar los mas rentables para recomendar

# + [markdown] tags=[]
# ### Encontrar grupo de __super lectores__
# Los super lectores son los que han rankeado mas veces y tienen mas pendientes, estos seran el principal listado de target para marketing directo.
# - Para encontrarlos se calcula la frecuencia en cada tabla, y se ordena
# - Debido a que son miles, para hacerles marketing directo se tomara el 2,5 percentil con mayor actividad
# -

df['super_rater_users'] = df['ratings'].groupby('user_id').agg({'user_id':'count','rating':['min','mean','max']})
df['super_rater_users'].sort_values([('user_id','count')], inplace=True, ascending=False)

df['super_rater_users'].describe()

df['super_rater_users'][ df['super_rater_users'][('user_id','count')] > 18.3+2*26.2 ]

# + tags=[]
df['super_to_read_users'] = df['to_read'].groupby('user_id').agg({'user_id':'count'})
df['super_to_read_users'].rename(columns={'user_id':'count'}, inplace=True)
df['super_to_read_users'].sort_values('count', inplace=True, ascending=False)
# -

df['super_to_read_users'].describe()

df['super_to_read_users'][ df['super_to_read_users']['count'] > 18.6+2*16 ]

# + [markdown] tags=[]
# ### Encontrar el grupo de __libros más rentables y mayor valor__ para promocionar (a los que lo marcaron por leer)
# -

# Si:
#
#     Volume - Volumen de ventas hasta el 2010
#     Value - Ventas determinadas por el volumen
#     RRP - Precio recomendado para minoristas
#     ASP - Precio promedio para venta
#     
# Entonces se puede derivar
#     
#     Venta promedio por unidad = Value / Volume
#     
# Que al calcular, es equivalente a `ASP`, salvo para 5 libros mostrados a continuacion.
# Asi para estos, se puede

df['top_books']['ValUnit'] = df['top_books'].Value / df['top_books'].Volume

df['top_books'][ abs(df['top_books'].ValUnit - df['top_books'].ASP) > 0.1] 

df['top_books']['profit'] = df['top_books'].RRP - df['top_books'].ASP

# Asi mismo, mientras mayor es el retorno mas rentable es de vender

df['top_books'].sort_values('profit',ascending=False)

# + [markdown] tags=[]
# ### Encontrar el grupo de libros __más marcado 'por leer'__
# Es razonable promocionar a todo publico -por ejemplo en un banner en la landing page del e-commerce o un cartel fisico en tienda- Los 5 libros que mas personas han demostrado interes por leer.
#
# Tambien es razonable esperar que para mejorar la rentabilidad de las ventas fisicas, exista un tamaño minimo de orden al proveedor para obtener un mejor precio 'mayorista', luego la estrategia deberia ser tomar solo los que cierto numero de personas (por ej 1000) demostraron interes por leer.
# -

df['most_to_read'] = df['to_read'].value_counts('book_id')

# Ajuste la estrategia segun el 'tamaño de lote conveniente'
minimo_para_promocion_por_leer = 1000
df['most_to_read'][ df['most_to_read'] >= minimo_para_promocion_por_leer ]

# + [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# ### Encontrar libros rankeados y por leer a la vez
# Existen 2 posibilidades:
# - Que lo leyó, rankeó y quiere repetir su lectura. El ranking compara libros leídos.
# - Que no lo leyó y rankeó para comparar cuales libros quisiera leer más que otros.  
#
# En ambos casos es buena idea recomendarle al cliente sobre este los libros con mayor ranking de este listado.

# + tags=[]
# equivalente a inner join
df['ranked_to_read'] = df['to_read'].merge( df['ratings'], on=['user_id','book_id'], how='inner')
df['ranked_to_read']
# -

# repitieron ranking y por leer?
if len( df['ranked_to_read'].user_id.unique() ) == len(df['ranked_to_read']):
    print('no hay usuarios con mas de un registro rankeado y por leer')
else:
    print('Existen usuarios que rankearon mas de un libro por leer\nSe priorizara tomando unicamente el de mayor ranking')
    df['ranked_to_read'].sort_values(ascending=False,by=['user_id','rating'],inplace=True)
    df['best_ranked_to_read'] = df['ranked_to_read'].groupby('user_id').first().reset_index() 

df['best_ranked_to_read']

# + [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# # Evaluacion, interpretacion y justificacion
# # Propuesta
# # Despliegue y Operaciones
# # Conclusiones

# + [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# # Anexos
# -

# ## Modelo de datos
# El archivo “books” contiene los datos generales de cada libro existente en la librería y además menciona el promedio de clasificación de cada libro de acuerdo a las votaciones y compras del cliente.  
# El archivo “top_books” contiene el top 20 de los libros más vendidos de acuerdo a una clasificación general.  
# El archivo “ratings” contiene los datos de los libros más votados por los clientes dentro del sitio web de la librería.  
# El archivo “to_read” contiene las recomendaciones que cada cliente o usuario realiza en el sitio web sobre libros para leer.  
#
# El archivo “books” contiene los siguientes datos:
#
#     Id - Identificador del registro
#     Book Id - Identificador del libro
#     Number Editions - Número de ediciones
#     ISBN - Clave estándar internacional del libro
#     ISBN13 - Clave estándar extendida internacional del libro
#     Authors - Autor del libro
#     Original Publication - Fecha de publicación
#     Original Title - Título original del libro
#     Title - Título del libro
#     Language Code - Clave de idioma del libro
#     Average Rating - Promedio de la clasificación del libro
#     Image - Enlace a la imagen de la portada del libro
#     Small Image - Enlace a la imagen en versión optimizada de la portada del libro.
#
# El archivo “top_books” contiene los siguientes datos:
#
#     Position - Posición del libro en la clasificación del libro
#     ISBN - Clave estándar extendida internacional del libro
#     Title - Título del libro
#     Author - Autor del libro
#     Imprint - Editorial
#     Publisher Group - Grupo Editorial
#     Volume - Volumen de ventas hasta el 2010
#     Value - Ventas determinadas por el volumen
#     RRP - Precio recomendado para minoristas
#     ASP - Precio promedio para venta
#     Binding - Tipo de encuadernación
#     Publ Date - Fecha de publicación
#     Product Class - Clasificación del libro
#     Classification - Clasificación General del libro
#
# El archivo “ratings” contiene los siguientes datos:
#
#     Book Id - Identificador del libro
#     User Id - Identificador del cliente/usuario que clasifico un libro
#     Rating - Nivel de clasificación del libro.
#
# El archivo “to_read” contiene los siguientes datos:
#
#     User Id - Identificador del cliente/usuario que clasifico un libro
#     Book Id - Identificador del libro
#

# ## Instrucciones del Caso
# Requerimientos
# 1. ¿Qué indicadores serían los más importantes a determinar de acuerdo a la información presentada? al menos 3 indicadores de desempeño (kpi's)
# 2. ¿Qué tipo de análisis sería el más adecuado y por qué? Justificar el tipo de análisis 
# 3. ¿Qué decisiones se podrían tomar basadas en los descubrimientos o inferencias de la información analizada? Para cada indicador de desempeño determinar al menos una decisión que debería tomar la librería. 
#
# Partes del informe
# - portada; 
# - un índice 
# - Introducción, en donde presentes las ideas que se revisarán en el proyecto.
# - Identificar los indicadores.
# - Seleccionar el tipo de analizar y justificar su uso.
# - Elaborar una propuesta de decisiones que la empresa podrá tomar en base a la información analizada.
# - Conclusiones, con un resumen de las propuestas que presentaste a la empresa.
#
# Cumplir con los requisitos establecidos en la Lista de cotejo señalada en la descripción de la actividad.
# 1. Contiene los datos de identificación de la empresa.		
# 2. Se incluye la descripción detallada de la estrategia de implementación.		
# 3. La estrategia de implementación contempla un proceso de evaluación. 		
# 4. Se mencionan las buenas prácticas que se deben utilizar para lograr la estrategia descrita. 		
# 5. Se justifica la estrategia de implementación. 
#
# Evaluar la práctica de 5 compañeros, los cuales te serán asignados automáticamente por la plataforma. Si no cumples este requisito no se liberará la evaluación de tu práctica individual.


